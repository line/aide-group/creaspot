import tkinter as tk
from PIL import Image, ImageTk
from tensorflow.keras.models import load_model
from collections import deque
import numpy as np
import argparse
import pickle
import cv2
import matplotlib.pyplot as plt
import json
from time import sleep, time
import datetime
import optparse
import os




CONF_R = "F01"
prob = "0%"

LABELS = ["F01", "F02", "F03", "F04", "F05", "F06", "F07", "F08", "F09", "F10", "F11", "F12", "F13", "F14", "F15",
          "F16", "F17", "F18", "AS00", "AS01", "AS02", "AS03"]


def convert_frame_to_time(count, fps):
    time = count / fps
    if time / 60 < 1:
        return "00:00:{:.2f}".format(time)
    elif time / 60 >= 1 and time / 60 < 60:
        minutes = int(time // 60)
        secondes = time / 60 - (time // 60)
        return "00:{}:{:.2f}".format(minutes, secondes)
    else:
        heures = int(time // 3600)
        minutes = int((time / 3600 - time // 3600) * 60)
        secondes = ((time / 3600 - time // 3600) * 60 - minutes) * 60
        return "{}:{}:{:.2f}".format(heures, minutes, secondes)


## Global
def toggle_start(start0, config0, frame0, prob0):
    global start, configjson, framejson, togglestart, toggleend, startjson, prob
    startjson = start0
    configjson = config0
    framejson = frame0
    prob = prob0
    togglestart = False
    toggleend = True

## Global
def toggle_end(end0):
    global endjson, json, togglestart, toggleend
    endjson = end0
    jsondict[framejson] = {
        "start": startjson, "end": endjson, "label": configjson, "prob": prob
    }
    toggleend = False
    togglestart = True



prev_config = ""

jsondict = {-1: {"start": "null", "end": "null", "label": "null", "prob": "null"},
            -2: {"start": "null0", "end": "null0", "label": "null0", "prob": "null0"}}
startjson = ""
endjson = ""
configjson = ""
framejson = 0
togglestart = True
toggleend = False


def get_processed_video(arguments):

    global CONF_R, prob, LABELS

    dir_path = os.path.dirname(os.path.realpath(__file__))
    print("PATH:",dir_path)

    #Load video
    print("VIDEO: ", arguments["input"])
    vs = cv2.VideoCapture(arguments["input"])

    print(vs.read())

    fps = vs.get(cv2.CAP_PROP_FPS)
    print("FPS:", fps)


    total = int(vs.get(cv2.CAP_PROP_FRAME_COUNT)) 
    
    print("TOTAL NUMBER OF FRAMES: ", total)
    print("IN SHOW FRAME")

    #Initialize video writer.
    writer = None

    #Array for predictions
    A = [[0] for i in range(len(LABELS))]
    print(A)
    
    print("[INFO] loading model and label binarizer...")
    #Loading trained model
    model = load_model(arguments["model"], compile=False)
    
    #Loading labels
    lb = pickle.loads(open(arguments["label_bin"], "rb").read())
    
    mean = np.array([123.68, 116.779, 103.939][::1], dtype="float32")
    
    #Queue to save best labels
    Q = deque(maxlen=arguments["size"])

    #Width and Height
    (W, H) = (None, None)

    #Variables for estimating time:
    estimated_one_frame, estimated, t = 0, 0, 0

    #Saving the actual frame in frame.
    _, frame = vs.read()
    print(frame)
    #Count frames
    count, prev_config = 0, ""

    #While there are other frames in the video
    while _:

        print('.', end='', flush=True)
        count += 1
        if count == 2:
            t = time() 
        print(count, "/", total)

        #Getting the video weight and height
        if W is None or H is None:
            (H, W) = frame.shape[:2]

        #copy the actual frame
        output = frame.copy()

        #Prepare the frame
        frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
        frame = cv2.resize(frame, (224, 224)).astype("float32")
        frame -= mean

        #Get the prediction
        preds = model.predict(np.expand_dims(frame, axis=0))[0]
        
        #Add the prediction to the Queue
        Q.append(preds)

        #Show predictions
        for i in range(len(preds)):
            A[i].append(preds[i])

        results = np.array(Q).mean(axis=0)

        # i the max of results
        i = np.argmax(results)

        # j the second max
        results_j = np.delete(results, i)
        j = np.argmax(results_j)

        #labels
        label_j = lb.classes_[j]
        label = lb.classes_[i]

        #Setting configuration
        if prev_config == "":
            #print("cas1")
            CONF_R = label
            prev_config = CONF_R

        if label == prev_config:
            if label == CONF_R:
                #print("cas2")
                prev_config = CONF_R
            else:
                #print("cas3")
                prev_config = label
        if label != prev_config:
            if label == CONF_R:
                #print("cas4")
                prev_config = CONF_R
            else:
                #print("cas5")
                CONF_R = label
                prev_config = CONF_R

        #Saving in json format
        global toggleend, togglestart
        global final_elem, before_final_elem
        [final_elem, before_final_elem] = sorted(jsondict.keys(), reverse=True)[0:2]
        final_elem = jsondict[final_elem]['label']
        before_final_elem = jsondict[before_final_elem]['label']
        if bool(jsondict) == True and final_elem != CONF_R and final_elem != before_final_elem:
            if togglestart == True and toggleend == False:
                togglestart = False
                toggle_start(convert_frame_to_time(count, fps), CONF_R, count,
                             "{:.2f}%".format(round(results[i], 2) * 100))
                #print(jsondict)
            elif toggleend == True and togglestart == False:
                toggle_end(convert_frame_to_time(count, fps))
                toggleend = False
                
        #Best and second best labels for the actual frame
        text = "Config: {}".format(label)
        text_j = "Config: {}".format(label_j)

        text_r = "Config: {}".format(CONF_R)

        #Subtitling the frame with the best label
        cv2.putText(output, text_r, (35, 50), cv2.FONT_HERSHEY_SIMPLEX,
                    1.25, (0, 255, 0), 5)

        #Initialize the writer
        if writer is None:
            fourcc = cv2.VideoWriter_fourcc(*"MP4V")
            writer = cv2.VideoWriter(arguments["output"], fourcc, 30, (W, H), True)
        #Save the frame
        writer.write(output)

        #Next frame
        _, frame = vs.read()
        if count == 2:
            t2 = time() 
            estimated_one_frame = t2 - t
            print(" t2: ", t2, " t: ", t)
            print("one frame estimated time: ", estimated_one_frame)
            estimated = total * estimated_one_frame
            print("estimated time: ", str(datetime.timedelta(seconds=estimated)))
        elif count > 2:
            print("estimated time: ", str(datetime.timedelta(seconds=(estimated - estimated_one_frame*(count-1)))))

    print("Video Saved.")

    print("[INFO] cleaning up...")
    del jsondict[-1]
    del jsondict[-2]
    elem=[]
    for elt,value in jsondict.items():
        if float(value['prob'][0:-1]) < arguments["proba"]:
            elem.append(elt)
    for e in elem:
        del jsondict[e]
    with open(arguments["path"], 'w', encoding='utf-8') as outfile:
        json.dump(jsondict, outfile, ensure_ascii=True)

        #Finish writing
    writer.release()
    vs.release()

def main():


    ap = argparse.ArgumentParser()

    ap.add_argument("-m", "--model", required=True, help="path to trained serialized model")
    ap.add_argument("-l", "--label-bin", required=True, help="path to  label binarizer")
    ap.add_argument("-i", "--input", required=True, help="path to our input video")
    ap.add_argument("-o", "--output", required=True, help="path to our output video")
    ap.add_argument("-s", "--size", type=int, default=128, help="size of queue for averaging")
    ap.add_argument("-p", "--proba", type=float, default=20.00, help="probabilty to write or not a configuration in the json file")
    ap.add_argument("-c", "--path", type=str, help="path to output json")

    args = vars(ap.parse_args())
    
    #print(arguments)
    get_processed_video(args)

if __name__ == "__main__":
    main()