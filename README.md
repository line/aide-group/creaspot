# creaspot

Analysing video of learning activities using machine learning

<a name='what'></a>

## Package repository

- Package files: <a target='_blank' href='https://gitlab.inria.fr/line/aide-group/creaspot'>https://gitlab.inria.fr/line/aide-group/creaspot</a>
- Package documentation: <a target='_blank' href='https://line.gitlabpages.inria.fr/aide-group/creaspot'>https://line.gitlabpages.inria.fr/aide-group/creaspot</a>
- Source files: <a target='_blank' href='https://gitlab.inria.fr/line/aide-group/creaspot/-/tree/master/src'>https://gitlab.inria.fr/line/aide-group/creaspot/-/tree/master/src</a>
- Version `0.1.1`
- License `CECILL-C`

## Installation

### User simple installation

- `npm install git+https://gitlab.inria.fr/line/aide-group/creaspot.git`

### Co-developper installation

- See the <a target='_blank' href='https://line.gitlabpages.inria.fr/aide-group/aidebuild/install.html#.install_as_developer'>related documentation</a>

Please refer to the <a target='_blank' href='https://line.gitlabpages.inria.fr/aide-group/aidebuild/install.html'>installation guide</a> for installation.

<a name='how'></a>

## Usage


See the [short presentation](https://gitlab.inria.fr/line/aide-group/creaspot/-/blob/master/doc/presentation.md) for more information.

### npm script usage
```
npm install --quiet : installs all package dependencies and sources.
npm run build: builds the different compiled, documentation and test files.
npm test     : runs functional and non-regression tests.
npm run clean: cleans installation files.
```

<a name='dep'></a>

## Dependencies

- <tt>aideweb: <a target='_blank' href='https://line.gitlabpages.inria.fr/aide-group/aideweb'>Javascript server side node express utilities for web applications and service</a></tt>
- <tt>express-ipfilter: <a target='_blank' href='https://www.npmjs.com/package/express-ipfilter'>A light-weight IP address based filtering system</a></tt>

## devDependencies

- <tt>aidebuild: <a target='_blank' href='https://line.gitlabpages.inria.fr/aide-group/aidebuild'>Builds multi-language compilation packages and related documentation.</a></tt>

<a name='who'></a>

## Authors

- Ali Issaoui&nbsp; <big><a target='_blank' href='mailto:ali-issaoui@outlook.fr'>&#128386;</a></big>
- Younes Jallouf&nbsp; <big><a target='_blank' href='mailto:younes.jallouf@ensg.eu'>&#128386;</a></big>
- Elie-Alban Lescout&nbsp; <big><a target='_blank' href='mailto:elie-alban.lescout@ensg.eu'>&#128386;</a></big>
- Hugo Tardy&nbsp; <big><a target='_blank' href='mailto:hugo.tardy@ensg.eu'>&#128386;</a></big>
- Thierry Viéville&nbsp; <big><a target='_blank' href='mailto:thierry.vieville@inria.fr'>&#128386;</a></big>
