### Update brew
``` 
brew update
```

### Git and python Install

install git :
``` 
brew install git
```
configure your github with the command:
```
git config --global user.name yourename
git config --global user.email youremail
git clone https://github.com/izaganami/Internship.git
cd Internship
```

Install python:
```
brew install python

```
Verify you have the right version:
```
python3 --version
```

#### Tensofrflow 1.9.0:

```
$ pip3 install tensorflow
```

#### fine-tunning-deeplearning
```
pip3 install virtualenv 

pip3 install opencv-python

pip3 install keras

pip3 install imutils

pip3 install sklearn

pip3 install scikit-video

pip3 install tk

```
#### data_preparation
```
pip install ffmpeg

pip3 install moviepy

pip3 install matplotlib

pip install pytest-shutil
```
#### subtitles utility
```
pip install pysubs2
```
### Some tests
```
cd /local/data/line/CubicScout/AI/fine-tunning-deeplearning
```
```
python main.py --help
python main.py --dataset C:/Users/youne/Desktop/Form-Type-Classifier/data --model output/activity.model --label-bin output/lb.pickle --epochs 50
python predict.py --help
```
Check the file below for more infos:
```
Internship/AI/fine-tunning-deeplearning-master/script-to-train
```
