#!/bin/cat

# Installs system elements to have the Web page service running

# Done once: Redirects the :80 port to :8080 to manage the http service without root privileges
firewall-cmd --permanent --direct --add-rule ipv4 nat PREROUTING 0 -p tcp --dport 80 -j REDIRECT --to-ports 8080
firewall-cmd --permanent --direct --get-all-rules

# Done if creaspot.service is installed or changes
cp src/creaspot.service /etc/systemd/system
systemctl daemon-reload
systemctl enable creaspot

# Done if any update on the web service files
systemctl restart creaspot
sleep 1
systemctl status creaspot
curl -X GET http://aide-line.inria.fr ; echo

# More
journalctl -r -u creaspot.service

# to clean : journalctl --rotate --vacuum-time=1s
