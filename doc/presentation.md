# The creaspot preliminary mechanism.

The present work offers a preliminary mechanism to analyze, in semi-automatic mode, video sequences of a the "CreaCube activity" of the [#CreaMaker](https://creamaker.wordpress.com) project. 

## Problem position

The CreaCube learning activity (see [CreaCube, a playful activity with modular robotics](https://www.researchgate.net/publication/329040986_CreaCube_a_playful_activity_with_modular_robotics) for details) engages the participant in building an autonomous vehicle able to move from a point A to another point B. In this context,the subject is faced with a set of modular robotic cubes. In order to understand this unknown technological artifacts, the participant manipulates them and builds them together in a configuration allowing to solve the task. See this [video](http://aide-line.inria.fr/public/doc/vid.mp4) for an illustration. The participant learning steps can be summarized as follows:

![CreaCube learning states](./images/learning-states.png "CreaCube learning states")

The task is code is based on the knowledge needed to solve and the initial states of the hardware and the final state for its success, as detailed here:

![CreaCube task coding](./images/task-coding.png "CreaCube task coding")

We can see the different observable taken into account, i.e. the possible configurations of the cubes (assembled in one way or another, or not), the discovery of affordances, i.e. the practical possibilities offered by an element (e.g. there is a switch, so it can be turned on), the different results obtained(e.g. at the motion level), but also elements linked to the subject, such as their emotions or their attitude (perseverance,abandonment) regarding the task. All these observables allows to modeling the task in the form of structured knowledge. The file model generated is a typed hierarchical format (JSON syntax, with both the raw and computed data as well as the description of each type of information and its relationships to other types) to allow its formal manipulation and the data analysis:

![CreaCube JSON data structure](./images/json-view.png "CreaCube JSON data structure")

Actually, video of the experiment are recorded and then manually coded off-line via a web interface. 
The aim here is to experiment to what extents a the manual logging could be replaced by a semi-automated interactive approach. 
This idea implies using a machine learning solution, feeding the script videos of the activity and returning the log of events recorded, along their name and timing.

The proposer paradigm is to use the algorithm as a pre-processing then manually corrected, and as a feedback reusing the error correction to improve the algorithm learning.

## Implemented solution

The mechanism is implemented as a [web service](https://line.gitlabpages.inria.fr/aide-group/creaspot/CreaspotService.html) accessible either via an [HTML page](http://aide-line.inria.fr/public/creaspot.html) or via [HTTP requests](https://gitlab.inria.fr/line/aide-group/creaspot/-/blob/master/src/wcreaspot.sh).

The algorithmic solution is schematized here:

![Network architecture](./images/network-archi.png "Network architecture")

Features are extracted using a Convolution Neural Network (CNN) from each video image frame and a Long Short Term Memory (LSTM) chain allows to take temporal relations into account. 

![CNN architecture](./images/resnet.png "CNN architecture")

The input is a pre-trained ResNet50 network, followed by AveragePooling2D to reduce the number of connections of the next convolution layers (downsampling), then Flatten (i.e. projecting the N-dimensional data container to one dimension), followed by a dense fully connected layer couple, with Rectified Linear Unit non-linearity, followed by a Dropout layer which selects a few neurons randomly so that their contribution in the next layer is temporarily ignored, avoiding overfitting, with finally a Dense Softmax layer allowing to select among the candidates output the most probable, output are normalized values between 0 and 1 allowing their interpretation as probabilities.

The LSTM layer are feed with the Dropout layer. The final result is of the following form:

![Algorithm output](./images/result.png "Algorithm output")

It is a sequence of epoch where a configuration detection is labeled with a given probability and a time interval.

## System training

A set of video has been decomposed in terms of images and labeled as illustrated here:

![Manual labeling of images](./images/json-illustration.png "Manual labeling of images")

The first training was carried out on a sample of 9000 images taken randomly by a script among the 180,000 images generated, training was done on a total of 500 epochs (i.e. an iteration on the whole data set), providing the following results: 

![Training result](./images/results.png "Training result")

with the following indicators

![Precision](./images/precision.png "Precision")

![Recall](./images/recall.png "Recall")

![F1-score](./images/f1-score.png "F1-score")

while the accuracy is the ratio of corrected predicted images. 

## Web service

The web service is based on the [aideweb semiware package](https://line.gitlabpages.inria.fr/aide-group/aideweb) and offers 
- An [URL based](https://line.gitlabpages.inria.fr/aide-group/creaspot/wcreaspot.html) service:
  - `curl -s -i -X POST -H "Content-Type: multipart/form-data" -F "uploadFile=$file" http://aide-line.inria.fr/creaspot/upload` to upload a video.
  - `curl -s http://aide-line.inria.fr/creaspot/run/predict` to run the prediction
  - `curl -s http://aide-line.inria.fr/creaspot/run/status` to get the prediction status
  - `wget -q http://aide-line.inria.fr/results/results.json` to download the result, when `predicted`, while `results.mp4` for visualization and `predict.log.txt` for the process logs are also available

## Web interface

- An [HTML based](http://aide-line.inria.fr/public/creaspot.html) interface to perform these operations:
 
[![Web service](./images/web-service.png "Web service")](http://aide-line.inria.fr/public/creaspot.html)

## References

- [Rapport de Stage, Cycle: ING2, ENSG. Analyse des traces d’une activité de résolution de problèmes, Younes Jallouf, Août 29, 2020](./rapport-de-stage-du-projet.pdf)

- [AIDE publications](https://team.inria.fr/mnemosyne/aide/#publis)
