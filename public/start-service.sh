#!/bin/bash

# Kills all previously node server running
kill -9 `ps -ef | grep '/bin/node src/server.js' | awk '{print $2}'` > /dev/null 2>&1

# Starts the conda environment
#eval "$(conda shell.bash hook)"
source $HOME/miniconda3/etc/profile.d/conda.sh
conda activate crea

# Starts the server
/bin/node src/server.js
