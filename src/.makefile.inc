#
### Warning : this .makefile.inc file is overwritten at each installation, do NOT edit
#

######################################################################################################
### This section implements the usage, and defines general variable and build
######################################################################################################

define USAGE
 Usage: make $$target
 Targets:
  usage: shows this usage.
  build: builds all compiled files.
  build_cpp: compiles C/C++ files, if any.
  build_gyp: compiles the node-gyp wrapper files, if any.
  build_python: generates a python wrapper of the compiled C/C++ files.
  build_public: compiles all public web site documentation files.
  test: runs automatic functional and non-regression tests.
  test  MAIN="" ARGV="": runs interactive tests.
  gtest MAIN="" ARGV="": runs interactive tests through gdb.
  vtest MAIN="" ARGV="": runs interactive tests through valgrind.
     MAIN: the *.C program name, 'test' by default.
     ARGV: the program arguments, if any.
  clean: cleans installation files.
  sync: syncs the files with the git repository.
  show: shows the doc using the current $BROWSER.
  install: installs the source files (normally only called by `npm install`, not via the `make` command).
  resinstall: clean and reinstall the whole package.
endef
export USAGE

usage:
	@echo "$$USAGE"

# Detects if used as a module or as root installation and defines the the destination accordingly
ifeq (node_modules,$(notdir $(patsubst %/,%,$(dir $(patsubst %/,%,$(dir $(shell pwd)))))))
DESTDIR = ../..
MODULE  = true
else
DESTDIR = ../node_modules
endif

# Package name
NAME=$(notdir $(patsubst %/,%,$(dir $(shell pwd))))

build: build_cpp build_gyp build_python build_public $(BUILD)
	@-git status -s

######################################################################################################
### This section implements C/C++ compilation
######################################################################################################

ifneq (,$(join $(wildcard *.hpp),$(wildcard *.cpp)))
ifeq (,$(shell grep '"build_cpp" *: *false' ../package.json))

### [A] Source file compilation generating a library and executable

ifndef CXX
export CXX = c++
endif

AIDE_CCFLAGS = -g -fPIC -Wall -std=c++17
ifeq (Darwin,$(shell uname -s))
AIDE_CCFLAGS += -D ON_MAC
ON_MAC = true
endif
ifeq (armv7l,$(shell uname -m))
AIDE_CCFLAGS += -D ON_RASPPI
ON_RASPPI = true
endif
ifeq (CYGWIN,$(shell uname -s | sed 's/_.*//'))
AIDE_CCFLAGS += -D ON_WIN
ON_WIN = true
endif

AIDE_INCDIRS = -I. $(patsubst %,-I../node_modules/%/src,$(AIDEDEPS)) $(patsubst %,-I../../%/src,$(AIDEDEPS)) $(patsubst %,-I../../../../%/src,$(AIDEDEPS))

DESTLIB = $(DESTDIR)/.lib/lib$(NAME).a

DESTBINS = $(patsubst %.C,$(DESTDIR)/.bin/%,$(wildcard *.C))

AIDE_LIB = -l$(NAME)

build_cpp: build_cpp_inc build_cpp_lib $(DESTBINS)

ifneq (,$(wildcard *.cpp))
build_cpp_lib: _build_dependencies $(DESTLIB)
else
build_cpp_lib:
endif

_build_dependencies:
	@for d in $(AIDEDEPS) ; do make -s -C $(DESTDIR)/$$d/src build_cpp_lib ; done

INCS = $(wildcard *.hpp)

build_cpp_inc :
	@mkdir -p $(DESTDIR)/.inc
	@cd $(DESTDIR)/.inc ; rm -f $(INCS) ; for f in $(INCS) ; do ln -s ../$(NAME)/src/$$f . ; done

$(DESTLIB): $(wildcard *.hpp) $(wildcard *.cpp)
	@$(CXX) -c $(CCFLAGS) $(AIDE_CCFLAGS) $(INCDIRS) $(AIDE_INCDIRS) $(wildcard *.cpp)
	@mkdir -p $(@D) ; ar -rc $@ *.o ; ar -s $@ ; rm -f *.o
	@$(MAKE) _concatenate_libs

### [B] Executable file(s) compilation

AIDE_LIBDIRS = -L$(DESTDIR)/.lib
AIDE_LIBS    = $(AIDE_LIB) $(patsubst %,-l%,$(AIDEDEPS)) -lstdc++ -lm

ifndef ON_WIN
AIDE_LIBS  += -lcurl
ifneq (,$(wildcard /usr/lib*/libpython3.12.so /usr/lib/*/libpython3.12.so))
PYTHON_LIB = -lpython3.12
else
ifneq (,$(wildcard /usr/lib*/libpython3.11.so /usr/lib/*/libpython3.11.so))
PYTHON_LIB = -lpython3.11
else
ifneq (,$(wildcard /usr/lib*/libpython3.10.so /usr/lib/*/libpython3.10.so))
PYTHON_LIB = -lpython3.10
else
ifdef ON_RASPPI
PYTHON_LIB = -lpython3.7m
endif
endif
endif
endif
endif
AIDE_LIBS += $(PYTHON_LIB)

$(DESTDIR)/.bin/%: %.C $(DESTLIB)
	@mkdir -p $(@D)
	@$(CXX) $(CCFLAGS) $(AIDE_CCFLAGS) $(INCDIRS) $(AIDE_INCDIRS) $*.C \
	   -o $@ $(LIBDIRS) $(AIDE_LIBDIRS) $(AIDE_LIBS) $(LIBS) 

# This dirty patch is used for node-gyp and on cygwin as a workaround for ld failure to find existing routines
_concatenate_libs:
	@cd $(DESTDIR)/.lib ; for f in *.a ; do ar -x $$f ; done ; ar -rc lib$(NAME).a *.o ; ar -s lib$(NAME).a ; rm -f *.o

else
build_cpp:
endif
else
build_cpp:
endif

######################################################################################################
### This section builds gyp-file mechanism
######################################################################################################

ifneq (,$(shell grep '"gypfile" *: *true' ../package.json))

build_gyp: ../node_modules/.build_gyp_done

../node_modules/.build_gyp_done: $(wildcard ../node_modules/.lib/*.a)
ifneq (,$(PYTHON_LIB))
	@cd .. ; cp binding.gyp binding.gyp~ ; sed  's/"-lpython[0-9.]*"/"$(PYTHON_LIB)"/' < binding.gyp~ > binding.gyp
endif
	@cd .. ; node-gyp rebuild --silent
	@mkdir -p $(@D) ; touch $@

else
build_gyp:
endif

######################################################################################################
## This section implements the python binding compilation
######################################################################################################

ifneq (,$(shell grep '"swig" *: *true' ../package.json))
ifneq (,$(shell which swig 2>/dev/null))

# Python wrapper compilation variables
PY_CXX      = c++
PY_CCFLAGS  = -I. -Wno-deprecated-declarations -std=c++17
## Here the different python versions in use
PY_CCFLAGS += -I/usr/include/python3.10 -I/usr/include/python3.11 -I/usr/include/python3.12
## Collect all dependencies
PY_DEPS     = $(wildcard ../node_modules/*/src)
PY_CCFLAGS += $(patsubst %, -I%,$(PY_DEPS))
PY_HPP     += $(wildcard *.hpp ../node_modules/*/src/*.hpp)
PY_CPP      = $(wildcard *.cpp ../node_modules/*/src/*.cpp)
## All used libraries
PY_LDFLAGS  = -lstdc++ -lcurl -lm
## The python target directory and python pat
export PYTHONPATH = ../build/python/site-packages
## The python command
PYTHON_COMMAND = python3

# Python wrapper compilation commands
build_python: ../build/python/site-packages/$(NAME)/$(NAME).py

../build/python/site-packages/$(NAME)/$(NAME).py : $(PY_HPP) $(PY_CPP)
#	echo 'Building $@ from $(PY_HPP) and $(PY_CPP)'
## Prepares the build folder
	@/bin/rm -rf $(PYTHONPATH)/$(NAME) ; mkdir -p $(PYTHONPATH)/$(NAME) 
## Builds the swig wrapping include file
	@(echo "%module $(NAME)"; echo "%{"; for f in $(PY_HPP); do echo "#include \"$$f\""; done; echo "%}" ; echo '%include "std_string.i"'; for f in $(PY_HPP); do echo "%include \"$$f\""; done) > $(PYTHONPATH)/$(NAME)/$(NAME).i
## Builds the python *.py and *.so module files
	swig -module $(NAME) -c++ -python -o $(PYTHONPATH)/$(NAME)/$(NAME).C $(PYTHONPATH)/$(NAME)/$(NAME).i
	$(PY_CXX) $(PY_CCFLAGS) -fPIC -c $(PY_INC) $(PYTHONPATH)/$(NAME)/$(NAME).C $(PY_CPP)
	$(PY_CXX) -shared -o $(PYTHONPATH)/$(NAME)/_$(NAME).so *.o $(LIBS) $(PY_LDFLAGS)
	@cp $(PYTHONPATH)/$(NAME)/$(NAME).py $(PYTHONPATH)/$(NAME)/__init__.py
	@chmod -R a+rx $(PYTHONPATH)
## Cleans compilation files
	@/bin/rm -f *.o

else
build_python:
endif
else
build_python:
endif

######################################################################################################
### This section defines the test sequences
######################################################################################################

# Here constructs the library path from the -L elements

EMPTY=
SPACE=$(EMPTY) $(EMPTY)
export PATH:=$(PATH):../../node_modules/.bin/:../node_modules/.bin:./node_modules/.bin
export LD_LIBRARY_PATH=$(subst $(SPACE),:,$(patsubst -L%,%,$(AIDE_LIBDIRS) $(LIBDIRS)))

ifeq (,$(MAIN))
MAIN = test
endif

TESTBIN = ./node_modules/.bin/$(MAIN)

test: 
ifneq (,$(wildcard test.C))
ifeq (,$(shell grep '"build_cpp" *: *false' ../package.json))
	@$(MAKE) build_cpp
	@cd .. ; $(TESTBIN) $(ARGV)
else
	@echo 'Not using test.C, in package.json "build_cpp" is false.'
endif
endif
	@if [ -f test.js ] ; then cd .. ; node ./src/test.js $(ARGV) ; fi
ifneq (,$(ARGV))
	@if [ -f test.html ] ; then \
	  if [ -z "$$BROWSER" ] ; then echo 'You must define the $$BROWSER variable' ; exit -1 ; fi ;\
	  $$BROWSER ./test.html ;\
         fi
endif
ifneq (,$(wildcard test.py))
ifneq (,$(wildcard ../build))
	@$(PYTHON_COMMAND) test.py
endif
endif
ifneq (,$(TEST))
	@make $(TEST)
endif
	@if [ -f test.sh ] ; then cd .. ; bash ./src/test.sh $(ARGV) ; fi


ifneq (,$(wildcard test.C))

# Runs via gdb 

gtest: build_cpp
	@cd .. ; (echo "run $(ARGV)" ; echo "echo --- backtrace ------------------------------------------------------------------------------\n"; echo "backtrace" ; echo "echo --- backtrace full -------------------------------------------------------------------------\n" ; echo "backtrace full" ; echo "echo --------------------------------------------------------------------------------------------\n"; echo "quit 0") > /tmp/a.cmd ; gdb -q $(TESTBIN) -x /tmp/a.cmd 2>&1 | grep -v '^\[Detaching after vfork from child process' ; ok=1

# Runs via valgrind

vtest: build_cpp
	@cd .. ; ulimit -s 100000 2>/dev/null ; export GLIBCXX_FORCE_NEW=1; valgrind --max-stackframe=100000000 --track-origins=yes $(TESTBIN) $(ARGV)

endif

######################################################################################################
### This builds all public files
######################################################################################################

WHERE = ../public

build_public: _beautify ../README.md $(patsubst %.js,$(WHERE)/%.js, $(filter-out test.js, $(wildcard *.js))) $(patsubst %.css,$(WHERE)/%.css, $(wildcard *.css)) $(WHERE)/index.html

######################################################################################################
### This section manages source files normalization and compacted version generation
######################################################################################################

# Reformat .js .css .hpp .cpp files in order to have a standard format

ifneq (aidebuild,$(NAME))
uncrustify_cfg = ../node_modules/aidebuild/src/
endif

_beautify:
	@for f in $(wildcard *.js) ; do cp -p $$f $$f~ ; touch $$f~ -r $$f ; js-beautify -q -s 2 -n -r $$f ; touch $$f -r $$f~ ; done
	@for f in $(wildcard *.css) ; do cp -p $$f $$f~ ; touch $$f~ -r $$f ; css-beautify -q -s 2 -n -r $$f ; touch $$f -r $$f~ ; done
	@if which uncrustify >/dev/null 2>&1 ; then for f in $(wildcard *.hpp) $(wildcard *.cpp) $(wildcard *.C) ; do cp -p $$f $$f~ ; touch $$f~ -r $$f ; uncrustify -q -c $(uncrustify_cfg)uncrustify.cfg -f $$f~ -o $$f ; touch $$f -r $$f~ ; done ; fi

# Generates public versions of the files

$(WHERE)/%.js: ./%.js
	@mkdir -p $(@D)
	@uglifyjs $^ -c -m toplevel=true > $@
	@chmod a+rx $@
	@if [ -f ../public/make-readme.js ] ; then if head -1 ../public/make-readme.js | grep '#!/usr/bin/env node' > /dev/null ; then cd $(DESTDIR)/.bin ; rm -f $* ; ln -s ../$(NAME)/public/$^ $* ; fi ; fi

$(WHERE)/%.css: ./%.css
	@mkdir -p $(@D)
	@tr "\n" " " < $^ | sed 's/\/\*[^\*]*\*\///g' | sed 's/  */ /g' | sed 's/ *\([:;{}]\) */\1/g' > $@

######################################################################################################
### This section builds the $(WHERE)/index.html web related files
######################################################################################################

DOC_FILES = \
 $(patsubst %.html,$(WHERE)/%.html, $(filter-out test.html,$(wildcard *.html))) \
 $(patsubst %.png,$(WHERE)/%.png, $(wildcard *.png)) \
 $(patsubst %.jpg,$(WHERE)/%.jpg, $(wildcard *.jpg)) \
 $(patsubst %.gif,$(WHERE)/%.png, $(wildcard *.gif)) \
 $(patsubst %.odg,$(WHERE)/%.png, $(wildcard *.odg)) \
 $(patsubst %.bib,$(WHERE)/%.bib, $(wildcard *.bib)) \
 $(patsubst %.sh,$(WHERE)/%.sh, $(wildcard *.sh)) \
 $(patsubst %.inc.tex,$(WHERE)/%.inc.tex, $(wildcard *.inc.tex)) \
 $(patsubst %.mpl,$(WHERE)/%.mpl, $(wildcard *.mpl)) \
 $(patsubst %.tex,$(WHERE)/%.tex, $(wildcard [A-Za-z]*.tex)) \
 $(patsubst %.tex,$(WHERE)/%.pdf, $(wildcard [A-Za-z]*.tex))

$(WHERE)/index.html: $(wildcard *.js) $(wildcard *.sh) $(wildcard *.hpp) $(WHERE)/jsdoc_config.json $(BUILD_README) $(DOC_FILES)
	@if [ \! -d ../node_modules/jsdoc2 ] ; then cd ../node_modules/aidebuild ; sh src/install.sh ; fi
	@echo '/** */' > ./.jsdoc2_header.js # added to run jsdoc even if no .js file but only .sh ou .hpp
	@node ../node_modules/aidebuild/src/jsdoc2/jsdoc2_readme.js < ../README.md > ../README.md~
	@../node_modules/.bin/jsdoc -c $(WHERE)/jsdoc_config.json -t ../node_modules/jsdoc2 -R ../README.md~ -d $(WHERE) ./.jsdoc2_header.js $(sort $(wildcard *.js))
	@rm ./.jsdoc2_header.js
ifneq (,$(shell which linkchecker 2>/dev/null))
	@if [ -z "`linkchecker -ocsv https://gitlab.inria.fr/line/aide-group 2>&1 | grep ConnectionError`" ] ;\
        then \
	  cd ../public; linkchecker --check-extern --no-warning -ocsv *.html 2> /dev/null | grep 'ConnectionError' | sed 's/^\([^;]*\);\([^;]*\);.*/Broken link "\1" in "\2"/' ;\
        fi
	@git add $@
endif

$(WHERE)/%.html : ./%.html
	@mkdir -p $(@D) ; cp -p $^ $@

$(WHERE)/%.png : ./%.png
	@mkdir -p $(@D) ; cp -p $^ $@

$(WHERE)/%.jpg : ./%.jpg
	@mkdir -p $(@D) ; cp -p $^ $@

$(WHERE)/%.gif : ./%.gif
	@mkdir -p $(@D) ; cp -p $^ $@

ifneq (,$(shell which libreoffice 2>/dev/null))
$(WHERE)/%.png : %.odg
	@libreoffice --headless --convert-to png --outdir $(@D) $^
endif

ifneq (,$(shell which pdflatex 2>/dev/null))
$(WHERE)/%.pdf : $(WHERE)/%.tex
	@mkdir -p $(@D) ; cp -p $^ $@
	@-cd $(@D) ; pdflatex $* ; bibtex $* ; pdflatex $* ; pdflatex $* ; rm -f *.aux $*.toc $*.blg $*.bbl $*.out $*.log
endif

$(WHERE)/%.sh : ./%.sh
	@mkdir -p $(@D) ; cp -p $^ $@

$(WHERE)/%.bib : ./%.bib
	@mkdir -p $(@D) ; cp -p $^ $@

$(WHERE)/%.inc.tex : ./%.inc.tex
	@mkdir -p $(@D) ; cp -p $^ $@

$(WHERE)/%.tex : %.tex
	@(cat ../node_modules/aidebuild/src/tex/preamble.tex ;\
	 echo '\begin{document}' ;\
	 cat $^ ;\
	 if grep '\\cite{' $^ > /dev/null ;\
	 then \
	   echo -n '\newpage {\scriptsize \bibliographystyle{apalike}\bibliography{' ;\
	   for f in $(wildcard *.bib) ;\
	   do echo -n "$$f," ;\
	   done ;\
	   echo '}' ;\
	 fi ;\
	 echo '\end{document}') > $@

ifneq (,$(shell which maple 2>/dev/null))
$(WHERE)/%.mpl : %.mpl
	@mkdir -p $(@D) ; cp -p $^ $@
	@-cd $(@D) ; maple $*.mpl | tee $@.out.txt
endif

define JSDOC_CONFIG
{
  "tags": {
    "allowUnknownTags": false
  },
  "source": {
    "includePattern": ".+.js$$"
  },
  "plugins": [
    "plugins/markdown",
    "plugins/jsdoc2"
  ],
  "markdown": {
    "parser": "gfm"
  },
  "templates": {
    "cleverLinks": true,
    "monospaceLinks": true,
    "default": {
      "outputSourceFiles": false
    }
  },
  "docdash": {
    "static": true, 
    "sort": false,
    "search": true,
    "collapse": true,
    "wrap": false
  }
}
endef
export JSDOC_CONFIG

$(WHERE)/jsdoc_config.json: makefile
	@mkdir -p $(@D)
	@echo "$$JSDOC_CONFIG" > $@

######################################################################################################
### This section allows to generate the ../README.md from the ../package.json
######################################################################################################

../README.md: ../package.json $(wildcard introduction.md)
ifeq (aidebuild,$(NAME))
	@cd .. ; ./src/make-readme.js
else
	@cd .. ; ./node_modules/aidebuild/public/make-readme.js
endif
	@git add $@

######################################################################################################
### This section implements installation mechanism : install, build, sync and show with clean
######################################################################################################

install: 
#	Refreshs the .makefile.inc file
ifneq (aidebuild,$(NAME))
	@wget -q https://gitlab.inria.fr/line/aide-group/aidebuild/-/raw/master/src/makefile -O ./.makefile.inc
endif
#	Installs makefile or script used defined commands
ifneq (,$(INSTALL))
	@make $(INSTALL)
endif
	@if [ -f install.sh ] ; then bash install.sh ; fi

sync:
	@git pull -q ; git commit -q -a -m 'sync from makefile' ; git status -s ; git push -q 2>&1 | grep -v '^remote:' ; ok=
	@$(MAKE) npm_publish

ifneq (,$(shell grep '"npm_publish" *: *true' ../package.json))
NPM_NAME = $(shell grep '"name" *:' ../package.json | sed 's/[^:]*[^"]*"\([^"]*\).*/\1/')
local_version = $(shell grep '"version" *:' ../package.json | sed 's/[^:]*[^"]*"\([^"]*\).*/\1/')

npm_publish:
	@cd .. ;\
	if wget -q --spider https://registry.npmjs.org/$(NPM_NAME) ; then \
	  online_version="`wget -q --output-document - https://registry.npmjs.org/$(NPM_NAME) | sed 's/.*\"latest\": *\"\([^\"]*\).*/\1\n/'`" ;\
	  if [ "$$online_version" = "$(local_version)" ] ;\
          then echo "https://www.npmjs.com/package/$(NPM_NAME) version '$(local_version)' is up to date" ;\
	  else echo "while https://www.npmjs.com/package/$(NPM_NAME) version is '$$online_version', local version is '$(local_version)', thus 'npm publish' may be run"  ;\
	  fi ;\
	else \
	  echo "You may which to publish https://www.npmjs.com/package/$(NPM_NAME) first, according to your package.json" ;\
	fi
else
npm_publish:

endif

reinstall:
	@make clean ; git pull ; cd .. ; npm install ; cd src ; make build test

clean:
	@cd .. ; /bin/rm -rf node_modules build package-lock.json ; find . -name '*~' -delete
ifneq (,$(CLEAN))
	@make $(CLEAN)
endif

ifndef BROWSER
BROWSER=firefox
endif

show:
	@$(BROWSER) ../public/index.html
