const express = require("express");
const execSync = require("child_process").execSync,
  execBatch = require("child_process").exec;

// Implements the creaspot web service

/** Implements the creaspot web service
 * 
 * - A POST [Uploadservice](https://line.gitlabpages.inria.fr/aide-group/aideweb/UploadService.html) is attached to the `/creaspot/upload` route, to upload a new input video.
 * - A GET service is attached to the `/creaspot/run/{clean,predict,status}`, routes.
 *   - `clean`: stops all processes and cleans all files
 *   - `predict`: starts the prediction (it takes a few minuts).
 *     - The `/results/{results.json,results.mp4,predict.log.txt}` files are available at the process end.
 *   - `status`: returns the process status
 *     - `cleaned`: after a clean
 *     - `loaded`: after a new input video has been uploaded.
 *     - `predicting`: when prediction is processing.
 *     - `predicted`: when prediction is done.
 *
 * @param {express} server The express server mechanism.
 * @class
 */
const CreaspotService = function(server) {

  // Implements the video upload service
  {

    const UploadService = require("../node_modules/aideweb/public/UploadService.js");

    new UploadService(server, "/creaspot/upload", "test_data", "input.mp4", function() {
      // Suppresses the last prediction achievement, since new data is loaded
      execSync("/bin/rm -f results/predict.end.ping");
    });
  }

  // Implements the run services
  {
    server.get("/creaspot/run/:action", function(request, response) {
      let action = request.params.action;
      const commands = ["clean", "status"],
        batches = ["predict", "train"];
      try {
        if (commands.includes(action)) {
          response.send(execSync("./src/" + action + ".sh"));
        } else if (batches.includes(action)) {
          execBatch("./src/" + action + ".sh");
          response.send("Starting " + action + "...");
        } else {
          response.status(400).send("Unknown /creaspot/run/:action '" + action + "'");
        }
      } catch (err) {
        response.status(500).send("Error running /creaspot/run/" + action);
      }
    });
  }

  // Opens the result directory
  server.use("/results", express.static("results"));
};

module.exports = CreaspotService;
