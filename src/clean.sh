#!/bin/sh

# Creates directory in case
mkdir -p test_data results 

# Kills all running scripts
killall -q train.sh
killall -q predict.sh

# Cleans all semaphore, log and result files
/bin/rm -f test_data/[0-9]*.* test_data/input.mp4 results/*

