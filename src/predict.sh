#!/bin/bash

# Tests that input is present
if [ -f results/train.run.ping ] ; then echo 'train already running, i quit' ; exit ; fi
if [ -f results/predict.run.ping ] ; then echo 'predict already running, i quit' ; exit ; fi

# Tests that input is present
if [ \! -f test_data/input.mp4 ] ; then echo 'test_data/input.mp4 not found' ; exit ; fi

# Starts the conda environment (done at the server level)
#eval "$(conda shell.bash hook)"
#conda activate crea

# Cleans the semaphore and log files (see status.sh)
/bin/rm -f results/results.mp4 results/results.json results/predict.*.* ; touch results/predict.run.ping

# Runs  in a log file
python AI/fine_tunning/src/predict.py --model AI/fine_tunning/output/activity.model --label-bin AI/fine_tunning/output/lb.pickle --input test_data/input.mp4 --output results/results.mp4 --size 128 --proba 10.00 --path results/results.json > results/predict.log.txt 2>&1

# Updates the semaphore files
/bin/rm -f results/predict.run.ping ; touch results/predict.end.ping
