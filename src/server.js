const express = require("express");
const server = express();

// Used to test if the server is alive 

server.get("/", function(request, response) {
  response.send("aide-line.inria.fr server is alive <b><a href='https://team.inria.fr/mnemosyne/en/aide/'>...</a></b>\n");
});

// Displays the html pages

server.use("/public", express.static("public"));

// Protects from unallowed IPS 
{
  const ipfilter = require("express-ipfilter").IpFilter;

  const allowedIPS = [
    "138.96.124.28", // aide-line.inria.fr
    "138.96.200.244" // kiriki.inria.fr 
  ];

  /*
    server.use(ipfilter(allowedIPS, { 
    mode: "allow",
    excluding: "/public"
    }));
  */

  // ref: https://www.npmjs.com/package/express-ipfilter
}

// Starts the services

const CreaspotService = require("./CreaspotService.js");
new CreaspotService(server);

const StopService = require("../node_modules/aideweb/public/StopService.js");
new StopService(server);

// Starts the service
server.listen(8080);
