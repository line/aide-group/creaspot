#!/bin/bash

# Service states:
# - cleaned                : all semaphore files are cleaned, no process running or achieved
# - loaded                 : a new file is loaded, no process running or achieved
# - training / trained     : the train.sh script is running / achieved
# - predicting / predicted : the predict.sh script is running / achieved

# Returns the service status depending on the semaphore files

if [ -f results/train.run.ping ]
then echo -n training
elif [ -f results/predict.run.ping ]
then echo -n predicting
elif [ -f results/predict.end.ping ]
then echo -n predicted
elif [ -f results/train.end.ping ]
then echo -n trained
elif [ -f test_data/input.mp4 ]
then echo -n loaded
else echo -n cleaned
fi
