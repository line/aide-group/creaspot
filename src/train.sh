#!/bin/bash

# Tests that input is present
if [ -f results/train.run.ping ] ; then echo 'train already running, i quit' ; exit ; fi
if [ -f results/predict.run.ping ] ; then echo 'predict already running, i quit' ; exit ; fi

# Starts the conda environment (done at the server level)
#eval "$(conda shell.bash hook)"
#conda activate crea

# Cleans the semaphore and log files (see status.sh)
/bin/rm -f results/train.*.* ; touch results/train.run.ping

# Runs in a log file
python AI/fine_tunning/src/main.py --dataset training_data/ --model AI/fine_tunning/output/activity.model --label-bin AI/fine_tunning/output/lb.pickle --epochs 50 > results/train.log.txt 2>&1 

# Updates the semaphore files
/bin/rm -f results/train.run.ping ; touch results/train.end.ping
