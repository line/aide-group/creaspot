#!/bin/bash

# /** 
#  * @class wcreaspot
#  * @description Command line connection to the creaspot web service
#  * ```
#  *     Usage: ./wcreaspot.sh $command
#  *     Commands:
#  *       upload $file: Uploads a file on the creaspot web service.
#  *       predict:      Starts the prediction on the last uploaded file.
#  *       status:       Gets the web service status.
#  *       download:     Downloads the prediction, avalaible when the status is 'predicted'.
#  *       clean:        Cleans files and processes on the server.
#  * ```
#  */

case "$1" in

  upload )
    if [ -f "$2" ] 
    then curl -s -i -X POST -H "Content-Type: multipart/form-data" -F "uploadFile=@$2" http://aide-line.inria.fr/creaspot/upload | grep -v ''
    elif [ -z "$2" ] ; then $0
    else echo "File not found: '$2'"
    fi
  ;;

  predict )
    curl -s http://aide-line.inria.fr/creaspot/run/predict ; echo
  ;;

  status )
    echo -n 'Status: ' ; curl -s http://aide-line.inria.fr/creaspot/run/status ; echo
  ;;

  download )
    if [ "`curl -s http://aide-line.inria.fr/creaspot/run/status`" = "predicted" ]
    then
        mkdir -p results
	wget -q -P results http://aide-line.inria.fr/results/results.mp4
	wget -q -P results http://aide-line.inria.fr/results/results.json
	wget -q -P results http://aide-line.inria.fr/results/predict.log.txt
        echo "Dowloaded in ./results/{results.{json,mp4},predict.log.txt}"
    else echo "No predicted results available"
    fi
  ;;

  clean )
    curl -s http://aide-line.inria.fr/creaspot/run/clean ; echo
  ;;

  # Shows usage if an undefined command
  * ) grep '^#  \*    ' $0 | sed 's/#  \*     //' ;;
esac
